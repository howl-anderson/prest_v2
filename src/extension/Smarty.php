<?php

namespace extension;

use \system\core\Path as Path;

require dirname(__FILE__) . '/smarty/Smarty.class.php';

class Smarty {

    public static $_smartyInstance = null;
    public static $_instance = null;
    public static $_callerObject = null;

    private function __construct()
    {
        //passed, not allowed init this class from outside.
    }

    public static function getInstance($callerObject)
    {
        if (self::$_instance == null) {
            $_smartyInstance = new \Smarty();
            $dirPrefix = Path::getPhysicalPath('\application\views\smarty');
            $_smartyInstance->template_dir = Path::join($dirPrefix, 'template');
            $_smartyInstance->compile_dir = Path::join($dirPrefix, 'compile');
            $_smartyInstance->config_dir = Path::join($dirPrefix, 'config');
            $_smartyInstance->cache_dir = Path::join($dirPrefix, 'cache');
            $_smartyInstance->left_delimiter = '<{';
            $_smartyInstance->right_delimiter = '}>';
            self::$_instance = new Smarty;
            self::$_callerObject = $callerObject;
        } 
        print 'smary in it';
        return self::$_instance;
    }

    public function assign($argv1, $argv2=null)
    {
        $this->_smartyInstance->assign($argv1, $argv2=null);
    }

    public function display($tpl)
    {
        $callerClassName = get_class(self::$_callerObject);
        $dirPrefix = Path::getPhysicalPath($callerClassName);
        $this->_smartyInstance->display($tpl);
    }
}
