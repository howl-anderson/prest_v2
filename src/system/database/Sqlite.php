<?php

namespace system\database;

class Sqlite
{
    private $_databaseBaseName = null;
    private $_databaseFqnName = null;
    private $_databaseHandle = null;
    private $_sqlResultObject = null;

    function __construct($callerObject, $databaseFile)
    {
        $this->_databaseBaseName = $databaseFile;
        $this->_databaseFqnName = \system\core\Data::getDataFile($callerObject, $this->_databaseBaseName);
        var_dump($this->_databaseFqnName);

        try {
            $this->_databaseHandle = new \PDO('sqlite:' . $this->_databaseFqnName);
        } catch (PDOException $e) {
            exit('error!');
        }
    }

    function __destruct()
    {
        $this->_databaseHandle = null;
    }

    function query($sqlString)
    {
        return $this->_databaseHandle->query($sqlString);
    }

    function fetchArray($sql)
    {
        $resultArray = array();
        $tmpResultObject = $this->query($sql);
        foreach($tmpResultObject as $tmpArray)
        {
            $resultArray[] = $tmpArray;
        }
        return $resultArray;
    }

    function execute($sql)
    {
        $this->_databaseHandle->exec($sql);
        var_dump($this->_databaseHandle->errorInfo());
    }

    function recordArray($sql)
    {
        return $this->query($sql)->fetchAll();
    }

    function recordCount($sql)
    {
        return count($this->RecordArray($sql));
    }

    function lastInsertID()
    {
        return $this->_databaseHandle->lastInsertId();
    }
}
