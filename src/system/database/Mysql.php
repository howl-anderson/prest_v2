<?php

namespace system\database;

class Mysql
{
    /**
     * database resource handle
     * @var resource
     */
    private $_dbHandle;

    /**
     * database result handle
     * @var resource
     */
    private $_result;

    /**
     * operate affected rows num
     * @var int
     */
    public $affectedRows;

    /**
     * select operate get result rows
     * @var int
     */
    public $numRows;

    /**
     * log handle
     * @var resource
     */
    private $_logHandle;

    /**
     * create log handle and acorrding the given confingure create a database object
     *
     * @param string $activeDatabase which database config do you want use
     * @return object return an instance of this class
     */
    public function __construct($activeDatabase='')
    {
        //$this->_logHandle = new \system\core\Log;
        $this->createDbHandle($activeDatabase);
        return $this;
    }

    /**
     * create database object according param or by default config
     * and set database charset
     *
     * @param string $activeDatabase which database config do you want use
     */
    private function createDbHandle($activeDatabase)
    {
        $dbConfigObject = new \application\config\core\Database();
        $dbConfig = $dbConfigObject->config();

        if (empty($activeDatabase)) {
            $activeDatabase = $dbConfig['defaultDatabase'];
        }
        $databaseConfig = $dbConfig[$activeDatabase];
        //print_r($databaseConfig);
        $this->_dbHandle = new \mysqli($databaseConfig['dbhost'], $databaseConfig['dbuser'], $databaseConfig['dbpass'], $databaseConfig['dbname']);
        $this->_dbHandle->query('SET NAMES `utf8`');
    }

    /**
     * query the database by execute the SQL in param
     *
     * @param string $queryString the sql string
     */
    public function query($queryString)
    {
        $queryString = $this->escapeString($queryString);
        //$this->_logHandle->debug($queryString);
        $this->_result = $this->_dbHandle->query($queryString); 
        if (!$this->_result) {
            printf('database error:%s\n', $this->_dbHandle->error);
        }
        $this->affectedRows =  $this->_dbHandle->affected_rows;
        if ($this->_result && $this->_result !== TRUE) {
            $this->numRows = mysqli_num_rows($this->_result);
        } else {
            $this->numRows = 0;
        }
    }

    /**
     * get query result object, you should query before you get result
     *
     * @return object mysql result object
     */
    public function result()
    {
        return $this->_result;
    }

    /**
     * return mysql fetch object result
     *
     * @return object mysql fetch object
     */
    public function fetchObject()
    {
        return $this->_result->fetch_object();
    }

    /**
     * return mysql fetch array result
     *
     * @return array mysql fetch array
     */
    public function fetchArray()
    {
        return $this->_result->fetch_array();
    }

    /**
     * return database link handle
     *
     * @return resource database handle
     */
    public function getHandle()
    {
        return $this->_dbHandle;
    }

    public function escapeString($sqlString)
    {
        //todo need do more
        //return $this->escape_string($sqlString);
        return $sqlString;
    }
}
