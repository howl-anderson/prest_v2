<?php

namespace system\core;

/**
 * the autoloader class for framework 
 * 
 * @package 
 * @version $id$
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Loader
{
    // difine the php file's extension
    const PHP_EXT = '.php';

    /**
     * check the class exists or not on physical disk
     * 
     * @param string $className 
     * @static
     * @access public
     * @return mixed
     */
    static public function classExists($className)
    {
        $classPhysicalPathName = \system\core\Path::getPhysicalPath($className)
                                 . self::PHP_EXT;
        if (file_exists($classPhysicalPathName)) {
            return $classPhysicalPathName;
        } else {
            return FALSE;
        }
    } 

    /**
     * try to load the class
     * 
     * @param string $className 
     * @static
     * @access public
     * @return void
     */
    public static function load($className)
    {
        if ($path = self::classExists($className)) {
            #TODO require or include?
            #see bootstrap.php
            require $path;
        } else {
            //something worry! sadlly~
            //TODO  this place need help
            #see bootstrap.php
            echo $className;
            header('Http-Status:404');
            exit('Loader:oh no 404~');
        }
    }

    /**
     * just tell the closesure load function load this class, empty function.
     * 
     * @static
     * @access public
     * @return void
     */
    public static function loadMe()
    {
        //nothing to do, on purpose
    }
}
