<?php

namespace system\core;

class Register
{
    private $_pool = array();
    private static $_instance = null;

    public static function getInstance()
    {
       if (self::$_instance === null) {
           self::$_instance = new self();
       }
       return self::$_instance;
    }

    private function __construct()
    {
        //nothing to do
    }

    private function __clone()
    {
        //nothing to do
    }

    public function put($source, $argumentOne, $argumentTwo = null, $group = null)
    {
        //todo: here need somthing
        if ($group === null) {
            $sourceArray = explode('\\', $source);
            $group = $sourceArray[0];
        }
        if (is_string($argumentOne)) {
            $this->_addToPool($group, $argumentOne, $argumentTwo, $source); 
        } else if (is_array($argumentOne)) {
            if ($argumentTwo === null) {
                foreach ($argumentOne as $key => $value) {
                    $this->_addToPool($group, $key, $value, $source); 
                }
            } else {
                //may be something wrong
            }
        }
    }

    private function _addToPool($group, $key, $value, $source)
    {
        $this->_pool[$group][$key] = array('value' => $value, 'source' => $source);
    }

    public function get($callerObject, $key, $group = null)
    {
        if ($group === null) {
            //todo
            //$group = $callerObject;
        }
        return $this->_pool[$group][$key]['value'];
    }
}
