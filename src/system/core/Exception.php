<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * handle for exception 
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Exception
{
    /**
     * exception handle function for framework
     * 
     * @param mixed $exception 
     * @static
     * @access public
     * @return void
     */
    public static function exceptionHandle($exception)
    {
        //TODO manage uncaught  exceptionHandle to log or other something
        echo 'Uncaught exception:' . $exception->getMessage() . "\n";
    }

    /**
     * set the framework exception handle
     * 
     * @static
     * @access public
     * @return void
     */
    public static function setExceptionHandle()
    {
        set_exception_handler('self::exceptionHandle');
    }
}
