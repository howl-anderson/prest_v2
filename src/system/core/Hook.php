<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * provide an simple common hook for class
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Hook
{
    // define the hook class's prefix
    const HOOK_CLASS_PREFIX = '\application\hook';

    /**
     * run
     * 
     * @param mixed $target 
     * @param mixed $argument 
     * @static
     * @access public
     * @return mixed
     */
    public static function run($target, $argument = NULL)
    {
        try {
            $archive = \system\core\Archive::get($target);
        } catch (\Exception $e) {
            throw new \Exception('archive get wrong');
        }
        $className = null;
        $methodName = null;

        list($className, $methodName) = $archive;

        $hookClass = self::HOOK_CLASS_PREFIX . '\\' . $className; 

        if (\system\core\Loader::classExists($hookClass)) {
            $hookObject = new $hookClass;
            if ($methodName === null) {
                $hookReturnValue = $hookObject->main($argument);
                return $hookReturnValue;
            } else {
                if (method_exists($methodName, $hookObject)) {
                    $hookReturnValue = $hookObject->$methodName($argument);
                    return $hookReturnValue;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
}
