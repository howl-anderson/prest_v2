<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * parse class's archive, just return the result, nothing more
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Archive
{
    /**
     * get the class's archive 
     * 
     * @param mixed $target 
     * @static
     * @access public
     * @return array
     */
    public static function get($target)
    {
        $className = null;
        $methodName = null;

        if (is_object($target)) {
            $className = get_class($target);
        } else if (is_string($target)) {
            $className = $target;
        } else if (is_array($target)) {
            $target = array_values($target);
            if (count($target) == 1) {
                $className = $target[0];
            } else if (count($target == 2)) {
                $className = $target[0];
                $methodName = $target[1];
            } else {
                throw new \Exception('too much values in array argument');
            }
        } else {
            throw new \Exception('unexcept argument type');
        }
        return array($className, $methodName);
    }
}
