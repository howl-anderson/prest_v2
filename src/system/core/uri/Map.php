<?php

namespace system\core\uri;

class Map
{
    static function run($uri)
    {
        $returnValue = \system\core\Hook::run(__CLASS__, $uri);
        return $returnValue;
    }
}
