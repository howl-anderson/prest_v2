<?php

namespace system\core;

class Log
{
    private $_writer = null;

    public function __construct($logWriter = null)
    {
        if ($logWriter === null) {
            $this->_writer = new namespace\log\File;
        } else if(is_object($logWriter)) {
            $this->_writer = $logWriter;
        } else {
            return false;
        }
    }

    public function write ($logGroup, $logContent)
    {
        if ($this->_writer->writer($logGroup, $logContent) === true) {
            return true;
        } else {
            exit('log system writer error');
        }
    }
}
