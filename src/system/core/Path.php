<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * Path 
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Path
{
    static private $_store = array();

    /**
     * input the path and put it's name to cache pool
     * 
     * @param mixed $setArray 
     * @static
     * @access public
     * @return true
     */
    public static function set($setArray)
    {
        self::$_store = array_merge(self::$_store, $setArray);
        return true;
    }

    /**
     * return the path by what you input
     * 
     * @param mixed $arrayKey 
     * @static
     * @access public
     * @return mixed
     */
    public static function get($arrayKey = null)
    {
        if ($arrayKey !== null) {
            return self::$_store[$arrayKey];
        } else {
            return self::$_store;
        }
    }

    /**
     * input logical path, return physical path
     * it don't care about what kind is the path, don't processor the file extension
     * 
     * @param mixed $logicalPath 
     * @static
     * @access public
     * @return string
     */
    public static function getPhysicalPath($logicalPath)
    {
        $pathMapArray = self::get();
        $elementArray = explode('\\', $logicalPath);
        if (empty($elementArray[0])) {
            array_shift($elementArray);
        }
        if (array_key_exists($elementArray[0], $pathMapArray)) {
            $physicalPathPrefix = $pathMapArray[$elementArray[0]];
            array_shift($elementArray);
            $physicalPath = $physicalPathPrefix
                          . '/'
                          . implode('/', $elementArray);
        } else {
            //something may include extsion dir,great feature,but may be ugly
            $physicalPathPrefix = $pathMapArray['extension'];
            $physicalPath = $physicalPathPrefix
                          . '/'
                          . implode('/', $elementArray);
        }
        return $physicalPath;
    }

    /**
     * just like python os.path.join()
     * concatnate the path, work like python os.path.join()
     * 
     * @param string $prePath
     * @param string $appendPath
     * @static
     * @access public
     * @return string
     */
    public static function join($prePath, $appendPath)
    {
        $clearedPrePath = rtrim($prePath, " \t\n\r\0\x0B/");
        //echo $clearedPrePath;
        $clearedAppendPath = rtrim($appendPath, " \t\n\r\0\x0B/");
        if (!empty($clearedPrePath)) {
            $resultPath = $clearedPrePath . '/' . $clearedAppendPath;
        } else {
            $resultPath = $clearedAppendPath;
        }
        return $resultPath;
    }
}
