<?php

namespace system\core;

class Data
{
    private static $_className = null;
    private static $_methodName = null;
    private static $_dataFileName = null;

    public static function getDataFile($target, $dataFileName)
    {
        self::$_dataFileName = $dataFileName;
        if (is_object($target)) {
            self::$_className = get_class($target);
        } else if (is_string($target)) {
            self::$_className = $target;
        } else if (is_array($target)) {
            $target = array_values($target);
            if (count($target) == 1) {
                self::$_className = $target[0];
            } else if (count($target == 2)) {
                self::$_className = $target[0];
                self::$_methodName = $target[1];
            } else {
                return false;
            }
        }

        $applicationPath = \system\core\Path::get('application');

        $dataFilePath = $applicationPath . '/data/' . str_replace('\\', '/', self::$_className) . (self::$_methodName ? '/' . self::$_methodName . '/' : '/') . self::$_dataFileName; 
        var_dump($dataFilePath);
        if (file_exists($dataFilePath)) {
            return $dataFilePath;
        } else {
            return false;
        }
    }
}
