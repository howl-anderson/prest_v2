<?php

namespace system\core\router;

class Map
{
    static function run($request)
    {
        $returnValue = \system\core\Hook::run(__CLASS__, $request);
        return $returnValue;
    }
}
