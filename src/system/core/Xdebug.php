<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * Xdebug
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Xdebug
{
    //TODO empty class need to add function
    public function __construct()
    {
    }
    
    public static function startTrace()
    {
    }

    public static function stopTrace()
    {
    }

    public static function start()
    {
    }

    public static function stop()
    {
    }

    public static function autostart()
    {
    }

    public static function autostop()
    {
    }
}
