<?php

namespace system\core;

/**
 * the database-based session class for framework 
 * 
 * @package 
 * @version $id$
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Session
{
    public $savePath = null;
    public $sessionName = null;
    public $dbHandle = null;

    public function __construct()
    {
        //$this->setSessionHandle();
    }

    /**
     * check the class exists or not on physical disk
     * 
     * @param string $className 
     * @
     * @access public
     * @return boolen
     */
    public function setSessionHandle()
    {
        //ini_set('session.save_handler', 'user');
        //ini_set('session.use_cookies', 0);
        session_set_save_handler(
            array($this, 'sessionOpen'),
            array($this, 'sessionClose'),
            array($this, 'sessionRead'),
            array($this, 'sessionWrite'),
            array($this, 'sessionDestroy'),
            array($this, 'sessionGc'));
        register_shutdown_function('session_write_close');
    } 

    public function sessionOpen($savePath, $sessionName)
    {
        $this->savePath = $savePath;
        $this->sessionName = $sessionName;
        //print $savePath;
        //print $sessionName;
        $dbHandle = new \system\database\Mysql();
        $this->dbHandle = $dbHandle;
        $this->dbHandle->query('USE `apis_session`');
        return true;
    }

    public function sessionclose()
    {
        return true;
    }

    public function sessionRead($sessionId)
    {
        $sqlString = "select `data` from `session` where `session_id` = '$sessionId'";
        //print $sqlString;
        $this->dbHandle->query($sqlString);
        $result = $this->dbHandle->fetchArray();
        return $result['data'];
    }

    public function sessionWrite($sessionId, $data)
    {
        //print $data;
        //print $sessionId;
        $sqlString = "select `data` from `session` where `session_id` = '$sessionId'";
        //print $sqlString;
        $this->dbHandle->query($sqlString);
        $result = $this->dbHandle->numRows;
        if ($result) {
            $sqlString = "update `session` set `data`='$data' where `session_id` = '$sessionId'";
        } else {
            $sqlString = "insert into `session`(`data`, `session_id`, `life_time`) values('$data','$sessionId'," . time() . ")";
        }
        //print $sqlString;
        $this->dbHandle->query($sqlString);
        $result = $this->dbHandle->numRows;
        return true;
    }

    public function sessionDestroy($sessionid)
    {
        $sqlString = "delete from `session` where `session_id` = '$sessionId'";
        //print $sqlString;
        $this->dbHandle->query($sqlString);
        $result = $this->dbHandle->numRows;
        return true;
    }

    public function sessionGc($lifetime)
    {
        echo 'gc:',$life_time;
        $sqlString = "delete from `session` where `life_time` < " . (time() - $lifetime);
        //print $sqlString;
        $this->dbHandle->query($sqlString);
        $result = $this->dbHandle->numRows;
        return true;
    }
}
