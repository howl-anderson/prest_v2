<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * Rewrite
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Rewrite
{
    public static $methodAllowed = array('POST', 'GET', 'PUT', 'DELETE');
    const METHOD_REWRITE_VAR = '_method';

    /**
     * rewriteMethod
     * 
     * @static
     * @access public
     * @return array
     */
    public static function rewriteMethod($requestInfo)
    {
        $requestInfo['method'] = strtoupper($requestInfo['method']);
        if (array_key_exists(self::METHOD_REWRITE_VAR, $requestInfo['var'])) {
            $newMethod = $requestInfo['var'][self::METHOD_REWRITE_VAR];
            $newMethod = strtoupper($newMethod);
            if (in_array($newMethod, self::$methodAllowed)){
                $requestInfo['method'] = $newMethod;
            }
        }
        return $requestInfo;
    }
}
