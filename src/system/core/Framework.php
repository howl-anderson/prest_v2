<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * Framework Class will check the maintain staus, give 503 on maintain before 
 * it exit or call some class and finally give controller to Router class.
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Framework
{
    /**
     * the only member function in class, order all the function of class
     * 
     * @static
     * @access public
     * @return void
     */
    public static function run()
    {
        $systemCoreConfig = \system\core\Config::get(__CLASS__);
        // switch to maintain status
        if ($systemCoreConfig['maintain']) {
            header('HTTP/1.1 503 Service Unavailable');
            exit();
        }
        \system\core\Xdebug::start();
        \system\core\Error::config();
        $errorHandle = new \system\core\Error();
        $errorHandle->setErrorHandle();
        \system\core\Exception::setExceptionHandle();
        \system\core\Output::startOutputBuffer();
        \system\core\Router::sendRoute();
        \system\core\Output::flushBuffer();
        \system\core\Xdebug::stop();
    }
}
