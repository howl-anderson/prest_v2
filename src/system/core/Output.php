<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * Output
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Output
{
    private static $_bufferContent;

    /**
     * start to buffer the output 
     * 
     * @static
     * @access public
     * @return true
     */
    public static function startOutputBuffer()
    {
        ob_start();
    }

    /**
     * stop to buffer and clean the output buffer 
     * 
     * @static
     * @access public
     * @return true
     */
    public static function endBuffer()
    {
        ob_end_clean();
    }

    /**
     * stop to buffer and flush the output to client
     * 
     * @static
     * @access public
     * @return true
     */
    public static function flushBuffer()
    {
        ob_end_flush();
    }

    /**
     * get the buffer content
     * 
     * @static
     * @access public
     * @return true
     */
    public static function getBuffer()
    {
        self::$_bufferContent = ob_get_contents();
        return self::$_bufferContent;
    }

    /**
     * set implicit flush to false
     * so only the flush is called, output will return to client 
     * 
     * @static
     * @access public
     * @return true
     */
    public static function closeImplicitFlush()
    {
        ob_implicit_flush(False);
    }
}
