<?php

namespace system\core;

class BitBucket
{
    /**
     * Any number and any type of arguments to call arbitrary,
     * methods always return true
     *
     * @param string $name the method name
     * @param array $arguments the arguments array
     * @return boolean TRUE this method always return true
     */
    public function __call($name, $arguments)
    {
        return TRUE;
    }

    /**
     * Any number and any type of arguments to call arbitrary static methods,
     * always return true
     *
     * @static
     * @param string $name the static method name
     * @param string $arguments the arguments array
     * @return boolean TRUE this method always return true
     */
    public static function __callStatic($name, $arguments)
    {
        return TRUE;
    }

    /**
     * no matter what you want get, it is always exists and equal  TRUE
     *
     * @param string $name property name
     * @return boolean TRUE this property always equal TRUE
     */
    public function __get($name)
    {
        return TRUE;
    }

    /**
     * no matter what you want set, it is always ok and return TRUE
     * @param string $name property name
     * @param mixed $value property value
     * @return boolean TRUE set property of this class always return TRUE
     */
    public function __set($name, $value)
    {
        return TRUE;
    }
}
