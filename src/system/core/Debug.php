<?php

namespace system\core;

class Debug
{
    public $config;
    public $writerObject = null;
    public $writerMethod = null;

    public function __construct()
    {
        $debugConfigObject = new \application\config\core\Debug;
        $this->config = $debugConfigObject->config();
        $this->writerObject = new \system\core\Log;
        $this->writerMethod = 'log';
    }
    public function setWriter($debugWriter = '', $writerMethod = 'log')
    {
        if (empty($debugWriter)) {
            $debugWriter = new \system\core\Log;        
        }
        if (empty($writerMethod)) {
            $writerMethod = 'log';
        }
        $this->writerObject = $debugWriter;
        $this->writerMethod = $writerMethod;
    }
    public function debug($debugLevel, $debugMessage)
    {
        $object = $this->writerObject;
        $method = $this->writerMethod;
        $object->$method($debugLevel, $debugMessage);
    }
}
