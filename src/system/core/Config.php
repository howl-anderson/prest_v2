<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * this class provide configuration of class or class/method from file 
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Config
{
    // config file dir in application
    const CONFIG_DIR = 'config';

    // cachePool for config cache
    static private  $_cachePool = array();

    /**
     * get the config
     * 
     * @param mixed $target 
     * @static
     * @access public
     * @return array
     */
    public static function get($target)
    {
        try {
            $archive = \system\core\Archive::get($target);
        } catch (\Exception $e) {
            throw new \Exception('archive get wrong');
        }
        $className = null;
        $methodName = null;

        list($className, $methodName) = $archive;

        if ($cacheVar = self::_getFromPool($className)) {
            if ($methodName == null) {
                return $cacheVar;
            } else {
                $configVar = $cacheVar;
                if (array_key_exists($methodName, $configVar)) {
                    return $configVar[$methodName];
                } else {
                    throw new \Exception('config no such method key');
                }
            }
        }

        $applicationPath = \system\core\Path::get('application');
        $php_ext = \system\core\Loader::PHP_EXT;
        $configFile = $applicationPath . '/' . self::CONFIG_DIR . '/' . str_replace('\\', '/', $className) . $php_ext; 
        if (file_exists($configFile)) {
            $configVar = include $configFile;
            self::addToPool($className, $configVar);
            if ($methodName == null) {
                return $configVar;
            } else {
                if (array_key_exists($methodName, $configVar)) {
                    return $configVar[$methodName];
                } else {
                    throw new \Exception('config no such method key');
                }
            }
        } else {
            //throw new \Exception('config file no exists');
            return false;
        }
    }

    /**
     * add the config to the cache pool
     * 
     * @param string $key  class name
     * @param array $value class config array 
     * @static
     * @access public
     * @return void
     */
    static public function addToPool($key, $value)
    {
        self::$_cachePool[$key] = $value;
    }

    /**
     * get config from cache pool
     * 
     * @param string $key class name 
     * @static
     * @access public
     * @return mixed
     */
    static public function _getFromPool($key)
    {
        if (array_key_exists($key, self::$_cachePool)) {
            return self::$_cachePool[$key];
        } else {
            return false;
        }
    }
}
