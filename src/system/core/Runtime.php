<?php

namespace system\core;

/**
 * Runtime 
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Runtime
{
    // const for application bootstrap controller and prefix class name
    const APPLICATION_CONTROLLER_PREFIX = '\\application\\controllers\\';
    const BOOTSTRAP_CLASS_NAME = '_bootstrap_';

    // store private var for caller
    private $_requestMethod = NULL;
    private $_requestUri = NULL;
    private $_requestVar = NULL;
    private $_processNodeStack = array();

    /**
     * switch the argument to the private var
     * 
     * @param stirng $requestMethod 
     * @param array $requestUri 
     * @param array $requestVar 
     * @access public
     * @return void
     */
    public function __construct($requestMethod, $requestUri, $requestVar)
    {
        $this->_requestMethod = $requestMethod;
        $this->_requestUri = $requestUri;
        $this->_requestVar = $requestVar;
    }

    /**
     * public start function for call
     * 
     * @access public
     * @return void
     */
    public function run()
    {
        $this->_run();
    }

    /**
     * the really main function for run
     * 
     * @access private
     * @return void
     */
    private function _run()
    {
        $bootstrapClass = self::APPLICATION_CONTROLLER_PREFIX
                        . self::BOOTSTRAP_CLASS_NAME;
        $this->_processNodeStack[] = array($bootstrapClass,
                                           $this->_requestMethod,
                                           $this->_requestUri,
                                           $this->_requestVar);
        list($nextNode,
             $nextRequestMethod,
             $nextRequestUri,
             $nextRequestVar) = $this->_runNode($bootstrapClass,
                                                $this->_requestMethod,
                                                $this->_requestUri,
                                                $this->_requestVar);
        while ($nextNode !== null) {
            $this->_processNodeStack[] = array($nextNode,
                                               $nextRequestMethod,
                                               $nextRequestUri,
                                               $nextRequestVar);
            list($nextNode,
                 $nextRequestMethod,
                 $nextRequestUri,
                 $nextRequestVar) = $this->_runNode($nextNode,
                                                    $nextRequestMethod,
                                                    $nextRequestUri,
                                                    $nextRequestVar);
        }
    }


    /**
     * run an processor Node use className and argument
     * 
     * @param string $className 
     * @param stirng $requestMethod 
     * @param array $requestUri 
     * @param array $requestVar 
     * @access private
     * @return void
     */
    private function _runNode($className, $requestMethod, $requestUri, $requestVar)
    {
        $initObject = new $className($requestMethod, $requestUri, $requestVar);
        $signal = $initObject->main(); 
        return $signal;
    }
    
    /**
     * __destruct
     * 
     * @access public
     * @return void
     */
    public function __destruct()
    {
    }
}
