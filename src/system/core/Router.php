<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * the request router 
 * 
 * @package 
 * @version $id$
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Router
{
    /**
     * this function will according to request infomation ,
     * give an request router info to the Runtime Class.
     * 
     * @static
     * @access public
     * @return void
     */
    public static function sendRoute()
    {
        $systemCoreConfig = \system\core\Config::get(__CLASS__);
        //TODO ugly! and should i need this?
        if ($systemCoreConfig['debug']) {
            $debugObject = new \system\core\Debug;
        } else {
            $debugObject = new \system\core\BitBucket;
        }
        $requestUri = \system\core\Uri::parseUri();
        $requestInfo = \system\core\Request::parseRequest();
        $requestMethod = strtolower($requestInfo['method']);
        $requestVar = array('get' => $requestInfo['get'],
                            'post' => $requestInfo['post'],
                            'var' => $requestInfo['var']);

        $requestArray = array('method' => $requestMethod,
                              'uri' => $requestUri,
                              'var' => $requestVar);
        $requestArray = namespace\router\Map::run($requestArray);

        $runtimeObject = new \system\core\Runtime($requestArray['method'],
                                                  $requestArray['uri'],
                                                  $requestArray['var']);
        $runtimeObject->run();
    }
}
