<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * request processor
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Request
{
    /**
     * parse Request information
     * 
     * @static
     * @access public
     * @return array
     */
    public static function parseRequest()
    {
        $requestInfo['method'] = $_SERVER['REQUEST_METHOD'];
        $requestInfo['post'] = $_POST;
        $requestInfo['get'] = $_GET;
        $requestInfo['file'] = $_FILES;
        $requestInfo['var'] = array_merge($_GET, $_POST);
        $requestInfo = \system\core\Rewrite::rewriteMethod($requestInfo);
        $requestInfo = namespace\request\Map::run($requestInfo);
        //var_dump($requestInfo);

        return $requestInfo;
    }
}
