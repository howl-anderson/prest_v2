<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * uri processor
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Uri
{
    private $_pathInfo;

    /**
     * get the path_info, store in private var 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->_pathInfo = $_SERVER["PATH_INFO"];
    }

    /**
     * this function will return a array that include request infomation.
     * 
     * @static
     * @access public
     * @return void
     */
    public static function parseUri()
    {
        $_pathInfo = $_SERVER['PATH_INFO'];
        $routerInfo = explode('/', $_pathInfo);
        array_shift($routerInfo);
        $routerInfo = namespace\uri\Map::run($routerInfo);
        return $routerInfo;
    }
}
