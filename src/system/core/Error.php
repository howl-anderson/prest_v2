<?php
/**
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */

namespace system\core;

/**
 * handle all the error 
 * 
 * @copyright Copyright (c) 2010-2012 howlanderson (http://www.howlanderson.net/)
 * @author howlanderson <u1mail2me@gmail.com> 
 * @license http://www.gnu.org/licenses/gpl-2.0.html  GPL v2
 */
class Error
{
    /**
     * make error thorw an exception
     * 
     * @param integer $errno 
     * @param string $errstr 
     * @param string $errfile 
     * @param integer $errline 
     * @static
     * @access public
     * @return void
     */
    public function errorException($errno, $errstr, $errfile, $errline)
    {
        throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
    }

    /**
     * error handle function
     * 
     * @param integer $errno 
     * @param string $errstr 
     * @param string $errfile 
     * @param integer $errline 
     * @static
     * @access public
     * @return void
     */
    public function errorHandle($errno, $errstr, $errfile, $errline)
    {
        //todo do something before translation controller to exception
        //$logContent = $errno . ',' . $errstr . ',' . $errfile . ',' . $errline;
        //$logerObject = new \system\core\Log;
        //$logerObject->write('error', $logContent);
        $this->errorException($errno, $errstr, $errfile, $errline);
    }

    /**
     * set error handle function
     * 
     * @static
     * @access public
     * @return void
     */
    public function setErrorHandle()
    {
        // TODO add code
        //set_error_handler(array($this, 'errorHandle'));
    }

    /**
     * config error setting
     * 
     * @static
     * @access public
     * @return void
     */
    public static function config()
    {
        // TODO add debug mode, product mode?
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }
}
