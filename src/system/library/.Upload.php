<?php
#/**
#  * @author howl u1mail2me@gmail.com
#  * @package \system\library
#  */
#
#namespace system\library;
#
#class Upload{
#    private $_config = null;
#    private $_errorMessage = array();
#    private $_fileAttribute = array();
#    
#    /**
#      *
#      */
#    public function __construct(){
#       $this->_config = new \application\config\library\Upload(); 
#    }
#
#    /**
#      *
#      */
#    public function getConfig(){
#        return $this->_config;
#    }
#
#    /**
#      * @param array $newConfig new config array
#      */
#    public function setConfig($newConfig){
#        $this->_config = $newConfig;
#    }
#    
#    /**
#      * validata the upload path existed and can writeable
#      * #none finsh yet aways return TRUE#
#      */
#    public function validateUploadPath(){
#        //!NOT FINSHED
#        return TRUE;
#    }
#
#    public function validateAllowedFileType($fileType){
#        return in_array($fileType,$this->_config['allowedFileType']);
#    }
#    
#    public function validateAllowedFileExtension($fileExtension){
#        return in_array($fileExtension,$this->_config['allowedFileExtension']);
#    }
#
#    public function getFileExtension($fileName){
#        if(($lastDotPos = strrpos($fileName,'.')) === FALSE){
#            return '';
#        }
#        $fileExtension = substr($fileName, $lastDotPos+1);
#        return $fileExtension;
#    }
#
#    /**
#      * @param array $errorMessage array content error message
#      */
#    public function setError($errorMessage){
#        $this->_errorMessage[] = $errorMessage;
#    }
#
#    /**
#      * @return array array of error message
#      */
#    public function getError(){
#        return $this->_errorMessage; 
#    }
#
#    /**
#      *
#      */
#    public function cleanError(){
#        $this->_errorMessage = array();
#    }
#
#    /**
#      * @param string $fieldName field name of file upload form field
#      * @return boolean sucessed finsh upload file or not
#      */
#    public function upload($fieldName){
#
#        // Is $_FILES[$field] set? If not, no reason to continue.
#        if ( ! isset($_FILES[$fieldName]))
#        {
#            $this->setError(array(0,'upload_no_file_selected'));
#            return FALSE;
#        }
#        
#        // Is the upload path valid?
#        if ( ! $this->validateUploadPath())
#        {
#            // errors will already be set by validate_upload_path() so just return FALSE
#            return FALSE;
#        }
#
#	// Was the file able to be uploaded? If not, determine the reason why.
#	if ( ! is_uploaded_file($_FILES[$fieldName]['tmp_name']))
#	{
#		$error = ( ! isset($_FILES[$fieldName]['error'])) ? 4 : $_FILES[$fieldName]['error'];
#		switch($error)
#		{
#			case 1:
#				// UPLOAD_ERR_INI_SIZE
#				$this->setError(array(1, 'upload_file_exceeds_limit'));
#				break;
#			case 2:
#				// UPLOAD_ERR_FORM_SIZE
#				$this->setError(array(2, 'upload_file_exceeds_form_limit'));
#				break;
#			case 3:
#				// UPLOAD_ERR_PARTIAL
#				$this->setError(array(3, 'upload_file_partial'));
#				break;
#			case 4:
#				// UPLOAD_ERR_NO_FILE
#				$this->setError(array(4, 'upload_no_file_selected'));
#				break;
#			case 6:
#				// UPLOAD_ERR_NO_TMP_DIR
#				$this->setError(array(6, 'upload_no_temp_directory'));
#				break;
#			case 7:
#				// UPLOAD_ERR_CANT_WRITE
#				$this->setError(array(7, 'upload_unable_to_write_file'));
#				break;
#			case 8:
#				// UPLOAD_ERR_EXTENSION
#				$this->setError(array(8, 'upload_stopped_by_extension'));
#				break;
#			default :
#				// 
#				$this->setError(array(9, 'upload_no_file_selected'));
#				break;
#		}
#		return FALSE;
#	}
#	
#	$this->_fileAttirbute['tempName'] = $_FILES[$fieldName]['tmp_name'];
#    $this->_fileAttirbute['size'] = $_FILES[$fieldName]['size'];
#    $this->_fileAttirbute['type'] = $_FILES[$fieldName]['type'];
#    $this->_fileAttirbute['name'] = $_FILES[$fieldName]['name'];
#    $this->_fileAttribute['storeName'] = null;
#    $this->_fileAttribute['extension'] = $this->;
#
#	// Is the file type allowed to be uploaded?
#	if ( ! $this->validateAllowedFileType())
#	{
#		$this->set_error('upload_invalid_filetype');
#		return FALSE;
#	}
#
#	// if we're overriding, let's now make sure the new name and type is allowed
#    if ($this->_file_name_override != '')
#	{
#        $this->file_name = $this->_prep_filename($this->_file_name_override);
#
#		// If no extension was provided in the file_name config item, use the uploaded one
#			if (strpos($this->_file_name_override, '.') === FALSE)
#			{
#				$this->file_name .= $this->file_ext;
#			}
#
#			// An extension was provided, lets have it!
#			else
#			{
#				$this->file_ext	 = $this->get_extension($this->_file_name_override);
#			}
#
#			if ( ! $this->is_allowed_filetype(TRUE))
#			{
#				$this->set_error('upload_invalid_filetype');
#				return FALSE;
#			}
#		}
#
#		// Convert the file size to kilobytes
#		if ($this->file_size > 0)
#		{
#			$this->file_size = round($this->file_size/1024, 2);
#		}
#
#		// Is the file size within the allowed maximum?
#		if ( ! $this->is_allowed_filesize())
#		{
#			$this->set_error('upload_invalid_filesize');
#			return FALSE;
#		}
#
#		// Are the image dimensions within the allowed size?
#		// Note: This can fail if the server has an open_basdir restriction.
#		if ( ! $this->is_allowed_dimensions())
#		{
#			$this->set_error('upload_invalid_dimensions');
#			return FALSE;
#		}
#
#		// Sanitize the file name for security
#		$this->file_name = $this->clean_file_name($this->file_name);
#
#		// Truncate the file name if it's too long
#		if ($this->max_filename > 0)
#		{
#			$this->file_name = $this->limit_filename_length($this->file_name, $this->max_filename);
#		}
#
#		// Remove white spaces in the name
#		if ($this->remove_spaces == TRUE)
#		{
#			$this->file_name = preg_replace("/\s+/", "_", $this->file_name);
#		}
#
#		/*
#		 * Validate the file name
#		 * This function appends an number onto the end of
#		 * the file if one with the same name already exists.
#		 * If it returns false there was a problem.
#		 */
#		$this->orig_name = $this->file_name;
#
#		if ($this->overwrite == FALSE)
#		{
#			$this->file_name = $this->set_filename($this->upload_path, $this->file_name);
#
#			if ($this->file_name === FALSE)
#			{
#				return FALSE;
#			}
#		}
#
#		/*
#		 * Run the file through the XSS hacking filter
#		 * This helps prevent malicious code from being
#		 * embedded within a file.  Scripts can easily
#		 * be disguised as images or other file types.
#		 */
#		if ($this->xss_clean)
#		{
#			if ($this->do_xss_clean() === FALSE)
#			{
#				$this->set_error('upload_unable_to_write_file');
#				return FALSE;
#			}
#		}
#
#		/*
#		 * Move the file to the final destination
#		 * To deal with different server configurations
#		 * we'll attempt to use copy() first.  If that fails
#		 * we'll use move_uploaded_file().  One of the two should
#		 * reliably work in most environments
#		 */
#		if ( ! @copy($this->file_temp, $this->upload_path.$this->file_name))
#		{
#			if ( ! @move_uploaded_file($this->file_temp, $this->upload_path.$this->file_name))
#			{
#				$this->set_error('upload_destination_error');
#				return FALSE;
#			}
#		}
#
#		/*
#		 * Set the finalized image dimensions
#		 * This sets the image width/height (assuming the
#		 * file was an image).  We use this information
#		 * in the "data" function.
#		 */
#		$this->set_image_properties($this->upload_path.$this->file_name);
#
#		return TRUE;
#	
#    }
#}
