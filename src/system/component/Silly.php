<?php

namespace system\component;

class Silly {
    public $templateDir = null;
    public $compileDir = null;
    public $configDir = null;
    public $cacheDir = null;
    public $leftDelimiter = null;
    public $rightDelimiter = null;
    public $tplPrefixDir = '';

    private $_varPool = array();

    public function __construct($caller)
    {
        $pathPrefix = \system\core\Path::getPhysicalPath('\application\views\silly');
        //echo $pathPrefix;
        $this->_initEnvriment($pathPrefix);
        $this->_loopPool = array();
    }

    /**
     * 为了更好的自定义和定制，这里将初始化环境的部分独立成函数，
     * 需要时，改写或者重载该函数
     */
    private function _initEnvriment($prefixDir='')
    {
        if (empty($prefixDir)) {
            $prefixDir = __DIR__;
        }
        //echo $currentDir;
        $this->templateDir = $this->_pathJoin($prefixDir, 'templates');
        //echo $this->templateDir;
        $this->compileDir = $this->_pathJoin($prefixDir, 'templates_c');
        $this->configDir = $this->_pathJoin($prefixDir, 'config');
        $this->cacheDir = $this->_pathJoin($prefixDir, 'cache');
    }

    /**
     * 为了更好的移植和独立出去，这里将用到的系统组件功能都包裹一下
     * 以后移植的时候，将这个函数的内容换成同功代码即可
     */
    private function _pathJoin($prePath, $appendPath)
    {
        return \system\core\Path::join($prePath, $appendPath);
    }

    public function assign($var1, $var2)
    {
        //TODO more feature will add
        $this->_varPool[$var1] = $var2;
    }

    public function display($tpl)
    {
        $tplPath = $this->_getTplPath($tpl);
        $compilePath = $this->_getCompilePath($tpl);
        //if (!file_exists($compilePath) || filemtime($compilePath) < filemtime($tplPath)) { 
            //echo "i compile the template";
            $opcode = $this->_compile($tplPath);
            file_put_contents($compilePath, $opcode);
        //}
        $html = $this->_eval($compilePath);
        echo $html;
    }

    private function _getTplPath($tpl)
    {
        $tplRelativePath = $this->_pathJoin($this->tplPrefixDir, $tpl) . '.tpl';
        //echo $tplRelativePath;
        //echo $this->templateDir;
        $tplPath = $this->_pathJoin($this->templateDir, $tplRelativePath);
        return $tplPath;
    }

    private function _getCompilePath($tpl)
    {
        $tplRelativePath = $this->_pathJoin($this->tplPrefixDir, $tpl) . '.php';
        //echo $tplRelativePath;
        //echo $this->templateDir;
        $tplPath = $this->_pathJoin($this->compileDir, $tplRelativePath);
        return $tplPath;
    }

    private function _runtime($opcode)
    {
        $html = $this->_eval($compilePath);
    }

    private function _compile($tpl)
    {
        $codeString = file_get_contents($tpl);
        //echo $codeString;
        $compiler = new namespace\silly\Compiler($this);
        $compiler->input($codeString);
        $opcodeString = $compiler->output();
        return $opcodeString;
    }

    private function _eval($filename)
    {
        ob_start();
        include $filename;
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    public function includeFile($file)
    {
        $tplPath = $this->_getTplPath($file);
        $fileContent = file_get_contents($tplPath);
        return $fileContent;
    }
}
