<?php

namespace system\component\silly;

class Virtualmachine
{
    public function execute($varPool, $opcode)
    {
        $this->_varPool = $varPool;
        $html = include $opcode;
        return $html;
    }
}
