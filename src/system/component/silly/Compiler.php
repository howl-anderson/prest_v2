<?php
namespace system\component\silly;

class Compiler
{
    public function __construct($runtime)
    {
        $this->_runtime = $runtime;
    }

    public function input($code)
    {
        $this->_compile($code);
    }

    private function _compile($code)
    {
        $code = $this->_compileInclude($code);
        //echo $code;
        $code = $this->_compileVar($code);
        $code = $this->_compileArray($code);
        //echo $code;
        //echo "<br>";
        $code = $this->_compileIf($code);
        $code = $this->_compileComment($code);
        //echo $code;
        $code = $this->_compileLoop($code);
        //echo $code;
        $this->comiledCode = $code;
    }

    private function _compileVar($code)
    {
        //TODO 这里应该规定不允许变量数字开头
        //TODO 应该允许自定义定界符
        //原有的毛毛虫模板，不能分析套嵌的模板变量，
        //这里将分化出一个专门的函数处理这种情况
        //本函数只能处理单独变量，处理套嵌变量由别的函数处理
        $pattern = '/<\{\$(\w+)\}>/';
        $code = preg_replace($pattern, "<?php echo \$this->_varPool['$1']; ?>", $code);
        return $code;
    }

    private function _compileInnerVar($code)
    {
        //本函数专门处理套嵌情况下的变量问题
        $pattern = '/\S*\$(\w+)\S*/';
        $code = preg_replace($pattern, "\$this->_varPool['$1']", $code);
        return $code;
    }

    private function _compileLoopInnerArray($code)
    {
        //本函数专门处理Loop情况下的数组问题
        $pattern = '/\s*\(\@([\w]+)\.([\w]+)\)\s*/';
        $code = preg_replace($pattern, "<?php echo \$this->_loopPool$1['$2']; ?>", $code);
        return $code;
    }

    private function _compileLoopInnerVar($code)
    {
        //本函数专门处理Loop情况下的变量问题
        $pattern = '/\s*\[\@(\w+)\]\s*/';
        $code = preg_replace($pattern, "<?php echo \$this->_loopPool$1; ?>", $code);
        return $code;
    }

    private function _compileIf($code)
    {
        //这里的分析方法不可靠，可以使用堆栈式的分析方法
        $if_patten = "/<\{\s*if\s+([^}]+)\}>/";
        $endif_patten = "/<\{\s*\/if\s*\}>/";
        while (preg_match($if_patten, $code, $match)) {
            $innerVar = $this->_compileInnerVar($match[1]);
            $code = preg_replace($if_patten, "<?php if( $innerVar ): ?>", $code);
            $code = preg_replace($endif_patten, "<?php endif; ?>", $code);
        }
        return $code;
    }

    private function _compileComment($code)
    {
        $patten = '/<\{#\}>([^{]*)<\{\/#\}>/';
        $code = preg_replace($patten, "<?php /* $1 */ ?>", $code);
        return $code;
    }

    private function _compileLoop($code)
    {
        $patten = '/<\{loop\s+\$(\w+)\s+\$(\w+)\s+\$(\w+)\s*\}>([^\{]*)<\{\/loop\}>/';
        while (preg_match($patten, $code, $match)) {
            $innerCode = $this->_compileLoopInnerVar($match[4]);
            $innerCode = $this->_compileLoopInnerArray($innerCode);
            $code = preg_replace($patten, "<?php foreach(\$this->_varPool['$1'] as \$this->_loopPool$2 => \$this->_loopPool$3): ?>$innerCode<?php endforeach; ?>", $code, 1);
        }
        return $code;
    }

    private function _compileInclude($code)
    {
        $patten = "/<\{\s*include\s+\"(\w+)\"\s*\}>/";
        while (preg_match($patten, $code, $match)) {
            $fileContent = $this->_runtime->includeFile($match[1]);
            $code = preg_replace($patten, $fileContent, $code, 1);
        }
        return $code;
    }

    private function _compileArray($code)
    {
        //本函数专门处理数组问题
        $pattern = '/\s*\(\$([\w]+)\.([\w]+)\)\s*/';
        $code = preg_replace($pattern, "<?php echo \$this->_varPool['$1']['$2']; ?>", $code);
        return $code;
    }

    public function output()
    {
        return $this->comiledCode;
    }
}
