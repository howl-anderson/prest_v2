<?php

namespace system\helper;

class Node
{
    static function relatedPath($classObject, $classPath)
    {
        if (is_object($classObject)) {
            $className = get_class($classObject);
            $classNamePathArray = explode("\\", $className);
            array_pop($classNamePathArray);
            $startPointPathArray = $classNamePathArray;

            $targetClassPathArray = explode("\\", $classPath);
            if (empty($targetClassPathArray[0])) {
                return $classPath;
            }
            $flag = count($targetClassPathArray);
            $arrayIndex = 0;
            while ($flag) {
                switch ($targetClassPathArray[$arrayIndex]) {
                    case '.':
                        $targetClassPathArray[$arrayIndex] = '';
                        break;
                    case '..':
                        array_pop($classNamePathArray);
                        break;
                    default:
                        //nothing to do
                        break;
                }

                $arrayIndex++;
                $flag--;
            }
            $purifiedTargetPathArray = array_filter($targetClassPathArray, function ($value){return !empty($value);});
            $finalPathArray = array_merge($classNamePathArray, $purifiedTargetPathArray);
            $finalPath = implode("\\", $finalPathArray);
            return $finalPath;
        } else {
            //throw some exception
        }
    }
}
