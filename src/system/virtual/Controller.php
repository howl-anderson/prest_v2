<?php

namespace system\virtual;

class Controller
{
    protected $_requestMethod = null;
    protected $_requestUri = null;
    protected $_requestVar = null;

    public function __construct($requestMethod, $requestUri, $requestVar)
    {
        $this->_requestMethod = $requestMethod;
        $this->_requestUri = $requestUri;
        $this->_requestVar = $requestVar;
    }

    public function main()
    {
        $this->{$this->_requestMethod}();
    }

    public function get()
    {
        $this->_httpResponseNotImplemented();
    }

    public function post()
    {
        $this->_httpResponseNotImplemented();
    }

    public function put()
    {
        $this->_httpResponseNotImplemented();
    }

    public function delete()
    {
        $this->_httpResponseNotImplemented();
    }

    public function header()
    {
        $this->_httpResponseNotImplemented();
    }

    public function options()
    {
        $this->_httpResponseNotImplemented();
    }

    public function trace()
    {
        $this->_httpResponseNotImplemented();
    }
    
    public function patch()
    {
        $this->_httpResponseNotImplemented();
    }

    private function _httpResponseNotAllowed()
    {
        \system\core\Http::sendResponseCode(405);
        \system\core\Output::endBuffer();
        exit();
    }

    private function _httpResponseNotImplemented()
    {
        \system\core\Http::sendResponseCode(501);
        \system\core\Output::endBuffer();
        exit();
    }

    protected function _registerResource($resource)
    {
        $registerObject = \system\core\Register::getInstance();
        $registerObject->put('', 'runtime', $resource, 'application');
    }

    protected function _getResource()
    {
        $registerObject = \system\core\Register::getInstance();
        return $registerObject->get($this, 'runtime', 'application');
    }
}
