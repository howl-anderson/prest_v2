<?php

namespace application\config\core;

class Database
{
    public function __construct()
    {
    }

    public function config()
    {
         /**
         * default setting
         */
        $db['defaultDatabase'] = 'default';
        $db['default']['driver'] = 'mysql';
        $db['default']['dbuser'] = 'root';
        $db['default']['dbpass'] = 'IWontTell';
        $db['default']['dbname'] = 'deepin'; 
        $db['default']['dbhost'] = 'localhost';

        return $db;
    }
}
