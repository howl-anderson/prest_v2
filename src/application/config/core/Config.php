<?php

namespace application\config\core;

class Config
{
    /**
     * return the config value
     *
     * @return array 
     */
    public function config()
    {
        return array(
                    'debug' => 1, # open the debug mode? option:boolean
                    'maintain' => 0 # maintain mode? option:boolean
                );
    }
}
