<?php

namespace application\config\library;

class Upload{
    public function __construct(){
        return array(
                'uploadDir'=>BASE_DIR . APPLICATION . '/upload/',
                'storeName'=>'',
                'allowedFileType'=>array(),
                'allowedFileExtension'=>array('zip','png','jpeg','jpg','doc','txt'),
                'allowedFileSize'=>1024*1024*256,
                'uploadFileOverwrite'=>0,
                'fileFilter'=>'',
                'fileSecurityFilter'=>''
                );
    }
}
