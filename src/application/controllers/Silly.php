<?php

namespace application\controllers;

class Silly extends \system\virtual\Controller
{
    public function get()
    {
        $silly = new \system\component\Silly;
        $silly->assign('var', 'hello, silly');
        $silly->assign('main', array(1=>'xx',2=>'yy'));
        $silly->assign('inner_if', 'hello, if');
        $silly->assign('if_flag', true);
        $silly->display('silly');
    }
}
