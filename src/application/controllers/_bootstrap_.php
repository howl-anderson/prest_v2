<?php

namespace application\controllers;

class _bootstrap_ extends \system\virtual\Controller
{
    public function main()
    {
        echo $this->_requestUri;
        $targetPath = \system\helper\Node::relatedPath($this, ucfirst(array_shift($this->_requestUri)));
        $this->_registerResource('oh~ register is good~');
        return array($targetPath, $this->_requestMethod, $this->_requestUri, $this->_requestVar);
    }
}
