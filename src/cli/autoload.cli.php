#!/usr/bin/env php
<?php
define('SRC_ROOT', '..');
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('APP_STATUS', 'dev');//dev,test,online

spl_autoload_register(__NAMESPACE__ . 'spl_autoload');
spl_autoload_register(function ($className) {
    //todo this function may need static store for singleton module
    $class = SRC_ROOT . '/' . str_replace('\\', '/', $className) . '.php';
    #echo $class . "\n";
    if (file_exists($class)) {
        require($class);
    } else {
        //check: the not exists class is belong to framework?
        $classNamespaceArray = explode('\\', $className);
        if (count($classNamespaceArray) >= 3) {
            //something worry! sadlly~
            //todo  this place need help
            header('Http-Status:404');
            exit;
        } else {
            //just pass to the next autoload function if any
        }
        //END check
    }
});
