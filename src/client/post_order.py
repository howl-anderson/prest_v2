import os

try:
    from httplib import HTTPConnection # python 2
except ImportError:
    from http.clinet import HTTPConnection # python 3

http_connection = HTTPConnection('restbucks.com')
http_connection.putrequest('POST','/order')
http_connection.putheader('Host', 'restbucks.com')
http_connection.putheader('Content-Type', 'application/xml')
http_connection.putheader('Content-Length', 278)

http_connection.endheaders()

http_connection.send('''<order xmlns="http://schemas.restbucks.com/order">
    <location>takeAway</location>
    <items>
        <item>
            <name>latte</name>
            <quantity>1</quantity>
            <milk>whole</milk>
            <size>small</size>
        </item>
    </items>
</order>''')
return_string = http_connection.getresponse()
print return_string.getheaders()
print return_string.status
print return_string.read()
http_connection.close()
