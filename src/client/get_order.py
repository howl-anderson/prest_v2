import os

try:
    from httplib import HTTPConnection # python 2
except ImportError:
    from http.clinet import HTTPConnection # python 3

http_connection = HTTPConnection('restbucks.com')
http_connection.putrequest('GET','/order/123')
http_connection.putheader('Host', 'restbucks.com')

http_connection.endheaders()

return_string = http_connection.getresponse()
print return_string.getheaders()
print return_string.status
print return_string.read()
http_connection.close()
