<?php
/**
 * this is the front bootstrap file for framework, do boot and loading job
 * 
 * this file will set base dir path for framework
 * register load function, register path, load the framework class.
 *  
 */

$pathMapArray = array('system' => __DIR__ . '/system',
                      'application' => __DIR__ . '/application',
                      'extension' =>  __DIR__ . '/extension');

// register the default spl_autoload function as first loading function
// TODO Should check it later, may no need this first load function,
// because this load function seem never works, almost use second funciton
// but for the class that code style not in our style, may have problom
// so think it over
spl_autoload_register(__NAMESPACE__ . 'spl_autoload');

// register an closesure function as second load function
spl_autoload_register(
    function ($className) {
        if (class_exists('\system\core\Loader', false)) {
            \system\core\Loader::load($className);
        } else {
            if (class_exists('\system\core\Path', false)) {
                $pathMapArray = \system\core\Path::get();
            } else {
                global $pathMapArray;
            }
            if (!isset($pathMapArray)) {
                //TODO this place need help
                exit('something wrong');
            }
            $classElementArray = explode('\\', $className);
            if (array_key_exists($classElementArray[0], $pathMapArray)) {
                $classPrePath = $pathMapArray[$classElementArray[0]];
                unset($classElementArray[0]);
                $class = $classPrePath .'/'. implode('/', $classElementArray) . '.php';
            } else {
                //something may include extsion dir,great feature,but may be ugly
                //TODO this feature is needed? well, take time think it over
                $classPrePath = $pathMapArray['extension'];
                $class = $classPrePath .'/'. implode('/', $classElementArray) . '.php';
            }
            if (file_exists($class)) {
                //TODO require or include? think it
                require $class;
            } else {
                //something worry! sadlly~
                //TODO  this place need help
                var_dump($className);
                var_dump($class);
                header('Http-Status:404');
                exit('oh no 404~');
            }
        }
    }
);

// register path
\system\core\Path::set($pathMapArray);

// destroy the global path var
unset($pathMapArray);

// load the loading class to replace the closesure load function
\system\core\Loader::loadMe();

// run the Framework class
\system\core\Framework::run();
