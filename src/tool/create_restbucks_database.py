#!/usr/bin/env python

import sqlite3

database_handle = sqlite3.connect('restbucks.db')

database_cursor = database_handle.cursor()

database_cursor.execute('''CREATE TABLE order(id integer, time integer, status text, list text)''')

database_handle.commit()
database_handle.close()
