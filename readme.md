this file should write in english, but i will write in chinese for expression more clearly
***
#什么是prest
prest是本人在业余时间用PHP编写的REST框架，用于搭建PHP环境的REST软件。由于个人认识和历史设计问题，prest分为两个版本。
#prest版本一
版本1用于提供简单、可靠、高效的架构；
#rest版本二
版本2用于提供高灵活、自由度更加大的架构;
#为什么要两个版本同时开发
为了更加大胆的追求新思维和新技术的实现，我决定采用两个版本同时齐头并进的开发策略。
#两个版本的移植性
两者在设计思想方面差异较大，可移植性不是很好，等待两个版本都趋于稳定之后，我将考虑如何让两者移植更加容易的事宜
#开发周期
我打算使用一年的时间，将prest开发到企业级稳定的水平，之后根据情况，为之选择一个发展道路
#license
本软件采用GPL2授权
