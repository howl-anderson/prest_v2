#application目录结构

    application/
    ├── cache
    ├── config
    │   ├── core
    │   │   ├── Config.php
    │   │   ├── Database.php
    │   │   ├── Debug.php
    │   │   └── Log.php
    │   ├── helper
    │   ├── library
    │   │   └── Upload.php
    │   └── package
    ├── controllers
    │   ├── screenshots
    │   │   ├── package
    │   │   │   └── ...
    │   │   ├── Package.php
    │   │   └── Screenshot.php
    │   ├── softcenter
    │   │   └── v1
    │   │       ├── Account.php
    │   │       ├── Analytics.php
    │   │       ├── avatar
    │   │       │   └── ...
    │   │       ├── Comment.php
    │   │       ├── images
    │   │       │   ├── yhpl_03.gif
    │   │       │   ├── yhpl_07.gif
    │   │       │   ├── yhpl_11.gif
    │   │       │   ├── yhpl_13.gif
    │   │       │   ├── yhpl_18.gif
    │   │       │   ├── yhpl_21.gif
    │   │       │   └── yhpl_24.gif
    │   │       ├── Mark.php
    │   │       ├── simple_html_dom.php
    │   │       ├── Soft.php
    │   │       ├── updateData.zip
    │   │       └── Userdata.php
    │   └── test
    │       ├── Hello.php
    │       ├── Import.php
    │       ├── Language.php
    │       └── Logtest.php
    ├── language
    │   ├── softcenter
    │   │   └── v1
    │   │       ├── en_US.UTF-8
    │   │       │   └── LC_MESSAGES
    │   │       │       └── comment.mo
    │   │       ├── zh_CN.UTF-8
    │   │       │   └── LC_MESSAGES
    │   │       │       └── comment.mo
    │   │       └── zh_TW.UTF-8
    │   │           └── LC_MESSAGES
    │   │               └── comment.mo
    │   └── test
    │       └── zh_CN.UTF-8
    │           └── LC_MESSAGES
    │               └── language.mo
    ├── log
    │   ├── core.log
    │   ├── debug.log
    │   ├── error.log
    │   ├── info.log
    │   ├── secure.log
    │   └── warn.log
    ├── models
    │   ├── screenshots
    │   │   ├── package
    │   │   │   └── Construct.php
    │   │   └── screenshot
    │   │       └── Construct.php
    │   ├── softcenter
    │   │   └── v1
    │   │       ├── comment
    │   │       │   └── Construct.php
    │   │       └── mark
    │   │           └── Construct.php
    │   └── test
    │       └── hello
    │           └── Construct.php
    ├── tmp
    │   └── language.php
    └── views
        ├── softcenter
        │   └── v1
        │       └── comment
        │           └── default.tpl
        └── test
            ├── hello
            ├── import
            └── language
