#system目录结构

    system/
    ├── component
    │   └── ...
    ├── core
    │   ├── BitBucket.php
    │   ├── Config.php
    │   ├── Controller.php
    │   ├── Database.php
    │   ├── Debug.php
    │   ├── Extension.php
    │   ├── Framework.php
    │   ├── Language.php
    │   ├── Log.php
    │   ├── Model.php
    │   ├── Request.php
    │   ├── Router.php
    │   ├── Security.php
    │   ├── Template.php
    │   └── Uri.php
    ├── extension
    │   └── template
    │       └── smarty3
    │           └── ...
    ├── helper
    │   └── Email.php
    ├── library
    │   └── Upload.php
    ├── module
    └── package

