#api请求/处理映射表
##评论功能
###获取评论
<table>
  <tr>
    <td>GET http://apis.linuxdeepin.com/softcenter/v1/comment?n={softName}</td>
  </tr>
</table>
请求参数说明
<table>
  <thead>
    <tr>
      <td>名称</td>
      <td>含义</td>
      <td>备注</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>softName</td>
      <td>软件的名字</td>
      <td>这是一个合法有效的软件名称</td>
    </tr>
  </tbody>
</table>
例如
<table>
  <tr>
    <td>GET http://apis.linuxdeepin.com/softcenter/v1/comment?n=deepin-software-center</td>
  </tr>
</table>
返回值说明
<table>
  <tr>
    <td>
      <pre>
        &lt;!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"&gt;
        &lt;html xmlns="http://www.w3.org/1999/xhtml"&gt;
        &lt;head&gt;
        &lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8" /&gt;
        &lt;title>Linux Deepin Software Center&lt;/title&gt;
        ...
      </pre>
    </td>
  </tr>
</table>
成功时返回
<table>
  <tr>
    <td>200 OK</td>
  </tr>
</table>
失败时返回
<table>
  <thead>
    <tr>
      <td>HTTP状态码</td>
      <td>错误原因</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>404</td>
      <td>软件不存在</td>
    </tr>
  </tbody>
</table>

###发送评论
<table>
  <tr>
    <td>
      POST http://apis.linuxdeepin.com/softcenter/v1/comment?n={softName} c={commentContent}
    </td>
  </tr>
</table>
请求参数说明
<table>
  <thead>
    <tr>
      <td>名称</td>
      <td>含义</td>
      <td>备注</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>softName</td>
      <td>软件的名字</td>
      <td>这是一个合法有效的软件名称</td>
    </tr>
    <tr>
      <td>commentContent</td>
      <td>评论的内容</td>
      <td>该内容需要安全过滤以及换行变换</td>
    </tr>
  </tbody>
</table>
例如
<table>
  <tr>
    <td>
      POST http://apis.linuxdeepin.com/softcenter/v1/comment?n=deepin-software-center c=mytest
    </td>
  </tr>
</table>
返回值说明
<table>
  <tr>
    <td>
      <pre>
      
      </pre>
    </td>
  </tr>
</table>

成功时返回
<table>
  <tr>
    <td>HTTP状态码：200 OK</td><td>返回值：1</td>
  </tr>
</table>
失败时返回
<table>
  <thead>
    <tr>
      <td>HTTP状态码</td><td>返回值</td><td>失败原因</td><td>备注</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>200</td>
      <td>not login yet</td>
      <td>用户没有（或者没有提交）有效的cookie</td>
      <td>这并不符合REST原则，是个需要改进的地方</td>
    </tr>
  </tbody>
</table>


##账户管理
###账户登录
<table>
  <tr>
    <td>POST http://apis.linuxdeepin.com/softcenter/v1/account user=
{userName}&pass={userPass}</td>
  </tr>
</table>
请求参数说明
<table>
<thead>
<tr>
<td>名称</td>
<td>含义</td>
<td>备注</td>
</tr>
</thead>
<tbody>
<tr>
<td>userName</td>
<td>用户的名字</td>
<td></td>
</tr>
<tr>
<td>userPass</td>
<td>用户的密码</td>
<td></td>
</tr>
</tbody>
</table>
例如
<table><tr><td>POST http://apis.linuxdeepin.com/softcenter/v1/account user=testuser&pass=testpass</td></tr></table>
返回值说明
<table>
<tr>
<td>
<pre>
</pre>
</td>
</tr>
</table>
成功时返回
<table><tr><td>HTTP状态码：200 OK</td><td>返回值：1</td><td>cookie变动：更新或者设置[softcenter_uid:xxxx][softcenter_auth:xxxxxxxxxx]</td></tr></table>
失败时返回
<table><thead><tr><td>HTTP状态码</td><td>返回值</td><td>失败原因</td><td>备注</td></tr></thead><tbody><tr><td>200</td><td>用户名或密码错误</td><td>0</td><td>这并不符合REST原则，是个需要改进的地方</td></tr></tbody></table>

###账户登出
<table><tr><td>GET http://apis.linuxdeepin.com/softcenter/v1/account?a=logout</td></tr></table>
请求参数说明
<table>
<thead>
<tr>
<td>名称</td>
<td>含义</td>
<td>备注</td>
</tr>
</thead>
</table>
例如
<table><tr><td>POST http://apis.linuxdeepin.com/softcenter/v1/account user=testuser&pass=testpass</td></tr></table>
返回值说明
<table>
<tr>
<td>
<pre>
</pre>
</td>
</tr>
</table>
成功时返回
<table><tr><td>HTTP状态码：200 OK</td><td>返回值：1</td><td>cookie变动：删除[softcenter_uid:xxxx][softcenter_auth:xxxxxxxxxx]</td></tr></table>
失败时返回
<table><thead><tr><td>HTTP状态码</td><td>返回值</td><td>失败原因</td><td>备注</td></tr></thead><tbody><tr><td>200</td><td>无法设置cookie</td><td>0</td><td>这并不符合REST原则，是个需要改进的地方</td></tr></tbody></table>
<table>

##用户数据
###用户数据获取
<table><tr><td>GET http://apis.linuxdeepin.com/softcenter/v1/userdata?s={$userResource}&uid={userId}</td></tr></table>
请求参数说明
<table>
<thead>
<tr>
<td>名称</td>
<td>含义</td>
<td>备注</td>
</tr>
</thead>
<tbody>
<tr>
<td>userResource</td>
<td>用户资源的名称</td>
<td>目前只支持avatar（头像）</td>
</tr>
<tr>
<td>userId</td>
<td>用户的ID</td>
<td></td>
</tr>
</tbody>
</table>
例如
<table><tr><td>GET http://apis.linuxdeepin.com/softcenter/v1/userdata?s=avatar&uid=4240</td></tr></table>
返回值说明
<table>
<tr>
<td>
<pre>

</pre>
</td>
</tr>
</table>
成功时返回
<table><tr><td>HTTP状态码:307 Temporary Redirect</td><td>返回值:localcation:apis.linuxdeepin.com/...</td></tr></table>
失败时返回
<table><thead><tr><td>HTTP状态码</td><td>错误原因</td></tr></thead><tbody><tr><td>404</td><td>资源不存在</td></tr></tbody></table>

##数据统计
###获取软件评分+评论数获取
<table><tr><td>POST http://apis.linuxdeepin.com/softcenter/v1/mark?n={softName}&t={formatType}</td></tr></table>
请求参数说明
<table>
<thead>
<tr>
<td>名称</td>
<td>含义</td>
<td>备注</td>
</tr>
</thead>
<tbody>
<tr>
<td>softName</td>
<td>软件名</td>
<td></td>
</tr>
<tr>
<td>formatType</td>
<td>格式类型</td>
<td>可选值有：comment vote</td>
</tr>
</tbody>
</table>
例如
<table><tr><td>GET http://apis.linuxdeepin.com/softcenter/v1/mark?n=deepin-software-center&t=vote</td></tr></table>
返回值说明
<table>
<tr>
<td>名称
</td>
<td>含义
</td>
<td>备注
</td>
</tr>
<tr>
<td>softName
</td>
<td>软件名
</td>
<td>
</td>
</tr>
<tr>
<td>valueOfVote
</td>
<td>评论得分
</td>
<td>
</td>
</tr>
<tr>
<td>valueOfB
</td>
<td>第二个项目的值
</td>
<td>
</td>
</tr>
</table>
<table>
<tr>
<td>
{{"{softName}:{valueOfVote},{valueOfB}"},...}
</td>
</tr>
</table>
成功时返回
<table><tr><td>HTTP状态码:200 OK</td></tr></table>
失败时返回
<table><thead><tr><td>HTTP状态码</td><td>错误原因</td></tr></thead><tbody><tr><td>404</td><td>资源不存在</td></tr></tbody></table>

###发送软件卸载和下载统计
<table><tr><td>POST http://apis.linuxdeepin.com/softcenter/v1/analytics?n={softName}&a={resourceType}</td></tr></table>
请求参数说明
<table>
<thead>
<tr>
<td>名称</td>
<td>含义</td>
<td>备注</td>
</tr>
</thead>
<tbody>
<tr>
<td>softName</td>
<td>软件名</td>
<td></td>
</tr>
<tr>
<td>resourceType</td>
<td>资源类型</td>
<td>可选值有：u d</td>
</tr>
</tbody>
</table>
例如
<table><tr><td>POST http://apis.linuxdeepin.com/softcenter/v1/analytics?n=deepin-software-center&a=u</td></tr></table>
返回值说明
<table>
<tr>
<td>
</td>
</tr>
</table>
成功时返回
<table><tr><td>HTTP状态码:200 OK</td></tr></table>
失败时返回
<table><thead><tr><td>HTTP状态码</td><td>错误原因</td><td>备注</td></tr></thead><tbody><tr><td>200</td><td>资源不存在等</td><td>此处无返回，违反了REST原则</td></tr></tbody></table>
