#数据库表结构

##admin
    +-------+-----------+------+-----+---------+----------------+
    | Field | Type      | Null | Key | Default | Extra          |
    +-------+-----------+------+-----+---------+----------------+
    | id    | int(10)   | NO   | PRI | NULL    | auto_increment |
    | uname | char(255) | NO   |     | NULL    |                |
    | pass  | char(255) | NO   |     | NULL    |                |
    | rname | char(255) | NO   |     | NULL    |                |
    +-------+-----------+------+-----+---------+----------------+
    +-------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------------+
    | Field | Type      | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment                  |
    +-------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------------+
    | id    | int(10)   | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |                          |
    | uname | char(255) | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references | 用户登录名，英文         |
    | pass  | char(255) | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references | 用户密码的md5值          |
    | rname | char(255) | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references | 用户真实姓名             |
    +-------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------------+

##avatar
    +-------------+-----------+------+-----+---------+----------------+
    | Field       | Type      | Null | Key | Default | Extra          |
    +-------------+-----------+------+-----+---------+----------------+
    | id          | int(11)   | NO   | PRI | NULL    | auto_increment |
    | uname       | char(255) | NO   |     | NULL    |                |
    | uid         | int(11)   | NO   |     | NULL    |                |
    | avatar_file | char(255) | NO   |     | NULL    |                |
    | extension   | char(255) | NO   |     | NULL    |                |
    | size        | int(11)   | NO   |     | NULL    |                |
    | md5         | int(11)   | NO   |     | 0       |                |
    +-------------+-----------+------+-----+---------+----------------+
    +-------------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+---------+
    | Field       | Type      | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment |
    +-------------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+---------+
    | id          | int(11)   | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |         |
    | uname       | char(255) | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references |         |
    | uid         | int(11)   | NULL            | NO   |     | NULL    |                | select,insert,update,references |         |
    | avatar_file | char(255) | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references |         |
    | extension   | char(255) | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references |         |
    | size        | int(11)   | NULL            | NO   |     | NULL    |                | select,insert,update,references |         |
    | md5         | int(11)   | NULL            | NO   |     | 0       |                | select,insert,update,references |         |
    +-------------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+---------+

##img
    +-----------+------------+------+-----+---------+----------------+
    | Field     | Type       | Null | Key | Default | Extra          |
    +-----------+------------+------+-----+---------+----------------+
    | id        | bigint(20) | NO   | PRI | NULL    | auto_increment |
    | sid       | bigint(20) | NO   |     | NULL    |                |
    | audit     | int(11)    | NO   |     | 0       |                |
    | md5       | char(255)  | NO   |     | NULL    |                |
    | timestamp | int(11)    | NO   |     | NULL    |                |
    | rid       | int(11)    | YES  |     | NULL    |                |
    +-----------+------------+------+-----+---------+----------------+
    +-----------+------------+-----------------+------+-----+---------+----------------+---------------------------------+------------------------------+
    | Field     | Type       | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment                      |
    +-----------+------------+-----------------+------+-----+---------+----------------+---------------------------------+------------------------------+
    | id        | bigint(20) | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |                              |
    | sid       | bigint(20) | NULL            | NO   |     | NULL    |                | select,insert,update,references | 软件ID                       |
    | audit     | int(11)    | NULL            | NO   |     | 0       |                | select,insert,update,references | 审核标记：0为未审核          |
    | md5       | char(255)  | ucs2_general_ci | NO   |     | NULL    |                | select,insert,update,references | 图片的md5值                  |
    | timestamp | int(11)    | NULL            | NO   |     | NULL    |                | select,insert,update,references | 该软件上传的时间戳           |
    | rid       | int(11)    | NULL            | YES  |     | NULL    |                | select,insert,update,references | 审核员ID                     |
    +-----------+------------+-----------------+------+-----+---------+----------------+---------------------------------+------------------------------+

##package
    +-----------+-----------+------+-----+---------+----------------+
    | Field     | Type      | Null | Key | Default | Extra          |
    +-----------+-----------+------+-----+---------+----------------+
    | id        | int(11)   | NO   | PRI | NULL    | auto_increment |
    | scount    | int(11)   | NO   |     | 0       |                |
    | sid       | int(20)   | NO   |     | NULL    |                |
    | sname     | char(255) | NO   |     | NULL    |                |
    | timestamp | int(11)   | NO   |     | NULL    |                |
    | md5       | char(255) | NO   |     | NULL    |                |
    | img       | char(255) | NO   |     | 0       |                |
    +-----------+-----------+------+-----+---------+----------------+
    +-----------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+------------------------------------------+
    | Field     | Type      | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment                                  |
    +-----------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+------------------------------------------+
    | id        | int(11)   | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |                                          |
    | scount    | int(11)   | NULL            | NO   |     | 0       |                | select,insert,update,references | 该软件截图的下载统计                     |
    | sid       | int(20)   | NULL            | NO   |     | NULL    |                | select,insert,update,references | 软件ID                                   |
    | sname     | char(255) | ucs2_general_ci | NO   |     | NULL    |                | select,insert,update,references | 软件名                                   |
    | timestamp | int(11)   | NULL            | NO   |     | NULL    |                | select,insert,update,references | 软件截图包生成时的时间戳                 |
    | md5       | char(255) | ucs2_general_ci | NO   |     | NULL    |                | select,insert,update,references | 该截图包的md5值                          |
    | img       | char(255) | ucs2_general_ci | NO   |     | 0       |                | select,insert,update,references | 该包的图片成员，json数组表示             |
    +-----------+-----------+-----------------+------+-----+---------+----------------+---------------------------------+------------------------------------------+

##pdb_cate
    +-------+--------------+------+-----+---------+----------------+
    | Field | Type         | Null | Key | Default | Extra          |
    +-------+--------------+------+-----+---------+----------------+
    | cid   | mediumint(9) | NO   | PRI | NULL    | auto_increment |
    | cname | varchar(45)  | YES  |     | NULL    |                |
    +-------+--------------+------+-----+---------+----------------+
    +-------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------+
    | Field | Type         | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment      |
    +-------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------+
    | cid   | mediumint(9) | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references | 分类ID       |
    | cname | varchar(45)  | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 分类名称     |
    +-------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------+

##pdb_comment
    +---------+--------------+------+-----+---------+----------------+
    | Field   | Type         | Null | Key | Default | Extra          |
    +---------+--------------+------+-----+---------+----------------+
    | id      | mediumint(9) | NO   | PRI | NULL    | auto_increment |
    | sid     | mediumint(9) | YES  |     | NULL    |                |
    | sname   | varchar(45)  | YES  |     | NULL    |                |
    | content | varchar(400) | YES  |     | NULL    |                |
    | cuid    | varchar(45)  | YES  |     | NULL    |                |
    | ctime   | int(10)      | YES  |     | NULL    |                |
    | cip     | varchar(45)  | YES  |     | NULL    |                |
    | cuname  | char(255)    | YES  |     | NULL    |                |
    +---------+--------------+------+-----+---------+----------------+
    +---------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------------------------------+
    | Field   | Type         | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment                                    |
    +---------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------------------------------+
    | id      | mediumint(9) | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references | 评论id                                     |
    | sid     | mediumint(9) | NULL            | YES  |     | NULL    |                | select,insert,update,references | 软件ID                                     |
    | sname   | varchar(45)  | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 软件名称                                   |
    | content | varchar(400) | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 评论内容                                   |
    | cuid    | varchar(45)  | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 评论用户ID,和深度论坛账号对应              |
    | ctime   | int(10)      | NULL            | YES  |     | NULL    |                | select,insert,update,references | 评论时间                                   |
    | cip     | varchar(45)  | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 评论IP                                     |
    | cuname  | char(255)    | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 用户名                                     |
    +---------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------------------------------+

##pdb_recording
    +-------+--------------+------+-----+---------+----------------+
    | Field | Type         | Null | Key | Default | Extra          |
    +-------+--------------+------+-----+---------+----------------+
    | id    | mediumint(9) | NO   | PRI | NULL    | auto_increment |
    | ip    | varchar(45)  | YES  |     | NULL    |                |
    | rtime | int(10)      | YES  |     | NULL    |                |
    +-------+--------------+------+-----+---------+----------------+
    +-------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------+
    | Field | Type         | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment      |
    +-------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------+
    | id    | mediumint(9) | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |              |
    | ip    | varchar(45)  | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references |              |
    | rtime | int(10)      | NULL            | YES  |     | NULL    |                | select,insert,update,references | 记录时间     |
    +-------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------+

##pdb_soft
    +-----------+--------------+------+-----+---------+----------------+
    | Field     | Type         | Null | Key | Default | Extra          |
    +-----------+--------------+------+-----+---------+----------------+
    | id        | mediumint(9) | NO   | PRI | NULL    | auto_increment |
    | name      | varchar(45)  | YES  |     | NULL    |                |
    | mark      | char(3)      | YES  |     | NULL    |                |
    | vote_nums | int(6)       | YES  |     | 0       |                |
    | down_nums | int(6)       | YES  |     | 0       |                |
    | uninstall | int(11)      | NO   |     | 0       |                |
    +-----------+--------------+------+-----+---------+----------------+
    +-----------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------+
    | Field     | Type         | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment            |
    +-----------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------+
    | id        | mediumint(9) | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |                    |
    | name      | varchar(45)  | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 软件名称           |
    | mark      | char(3)      | utf8_general_ci | YES  |     | NULL    |                | select,insert,update,references | 投票积分           |
    | vote_nums | int(6)       | NULL            | YES  |     | 0       |                | select,insert,update,references | 投票次数           |
    | down_nums | int(6)       | NULL            | YES  |     | 0       |                | select,insert,update,references | 下载次数           |
    | uninstall | int(11)      | NULL            | NO   |     | 0       |                | select,insert,update,references | 被卸载的次数       |
    +-----------+--------------+-----------------+------+-----+---------+----------------+---------------------------------+--------------------+

##pdb_soft_cate
    +--------+--------------+------+-----+---------+-------+
    | Field  | Type         | Null | Key | Default | Extra |
    +--------+--------------+------+-----+---------+-------+
    | sid    | mediumint(9) | NO   | PRI | NULL    |       |
    | cid    | varchar(45)  | NO   | PRI | NULL    |       |
    | sorder | tinyint(4)   | YES  |     | 0       |       |
    +--------+--------------+------+-----+---------+-------+
    +--------+--------------+-----------------+------+-----+---------+-------+---------------------------------+--------------+
    | Field  | Type         | Collation       | Null | Key | Default | Extra | Privileges                      | Comment      |
    +--------+--------------+-----------------+------+-----+---------+-------+---------------------------------+--------------+
    | sid    | mediumint(9) | NULL            | NO   | PRI | NULL    |       | select,insert,update,references | 软件ID       |
    | cid    | varchar(45)  | utf8_general_ci | NO   | PRI | NULL    |       | select,insert,update,references | 分类ID       |
    | sorder | tinyint(4)   | NULL            | YES  |     | 0       |       | select,insert,update,references | 软件排序     |
    +--------+--------------+-----------------+------+-----+---------+-------+---------------------------------+--------------+

##pdb_vote_ip
    +-------+------------+------+-----+---------+-------+
    | Field | Type       | Null | Key | Default | Extra |
    +-------+------------+------+-----+---------+-------+
    | ip    | bigint(20) | NO   | PRI | NULL    |       |
    | vtime | varchar(8) | NO   | PRI | NULL    |       |
    +-------+------------+------+-----+---------+-------+
    +-------+------------+-----------------+------+-----+---------+-------+---------------------------------+--------------+
    | Field | Type       | Collation       | Null | Key | Default | Extra | Privileges                      | Comment      |
    +-------+------------+-----------------+------+-----+---------+-------+---------------------------------+--------------+
    | ip    | bigint(20) | NULL            | NO   | PRI | NULL    |       | select,insert,update,references | 投票ip       |
    | vtime | varchar(8) | utf8_general_ci | NO   | PRI | NULL    |       | select,insert,update,references | 投票时间     |
    +-------+------------+-----------------+------+-----+---------+-------+---------------------------------+--------------+

##softcenter_mac
    +-------+------------+------+-----+---------+----------------+
    | Field | Type       | Null | Key | Default | Extra          |
    +-------+------------+------+-----+---------+----------------+
    | id    | bigint(20) | NO   | PRI | NULL    | auto_increment |
    | mac   | char(255)  | NO   |     | NULL    |                |
    | ip    | char(255)  | NO   |     | NULL    |                |
    | time  | int(11)    | NO   |     | NULL    |                |
    +-------+------------+------+-----+---------+----------------+
    +-------+------------+-----------------+------+-----+---------+----------------+---------------------------------+---------+
    | Field | Type       | Collation       | Null | Key | Default | Extra          | Privileges                      | Comment |
    +-------+------------+-----------------+------+-----+---------+----------------+---------------------------------+---------+
    | id    | bigint(20) | NULL            | NO   | PRI | NULL    | auto_increment | select,insert,update,references |         |
    | mac   | char(255)  | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references |         |
    | ip    | char(255)  | utf8_general_ci | NO   |     | NULL    |                | select,insert,update,references |         |
    | time  | int(11)    | NULL            | NO   |     | NULL    |                | select,insert,update,references |         |
    +-------+------------+-----------------+------+-----+---------+----------------+---------------------------------+---------+
